<?php
class Minus extends operation
{
    public function calc(float $first_number, float $second_number)
    {
        $this->answer=$first_number-$second_number;
        return $this;
    }

}