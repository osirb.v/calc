<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="css/style.css" rel="stylesheet">
    <title>pupermegСalc</title>
</head>
<body>
<form method="post" action="index.php">
    <div class="form">
    <div class="input_form">
        <input type="text" name="firstNumber" placeholder="First number">
        <input type="text" name="secondNumber"placeholder="Second number"></div>
        <div class="operation">
        <div class="switch"> <p>plus<input  type="radio"name="switch" value="plus"> </p></div>
        <div class="switch"> <p>minus<input type="radio"name="switch" value="minus"> </p></div>
        <div class="switch"> <p>mul<input   type="radio"name="switch" value="mul"> </p></div>
        <div class="switch"> <p>div<input   type="radio"name="switch" value="div"> </p></div></div>
        <button>Get answer</button>
    </div>
</form>

<?php

include_once 'Operation/Operation.php';
include_once 'Operation/Switch/Plus.php';
include_once 'Operation/Switch/Minus.php';
include_once 'Operation/Switch/Mul.php';
include_once 'Operation/Switch/Div.php';

$firstNumber = $_POST['firstNumber'];
$secondNumber = $_POST['secondNumber'];
$operation = $_POST['switch'];

if(!empty($firstNumber && $secondNumber && $operation)) {
    if (class_exists($operation)) {
        ucfirst($operation);
        $class = new $operation();
        $getAnswer = $class->calc($firstNumber,$secondNumber)->round(1)->answer();
    }
}

else {
    echo "<div class='switch'>nea</div>";
}

?>
<div class="switch"><?php echo $getAnswer; ?></div>

</body>

</html>